%global base_name Time-Local
%global base_version 1.35

Name:            perl-%{base_name}
Epoch:           2
Version:         %{base_version}0
Release:         2
Summary:         Efficiently compute time from local and GMT time
License:         GPL-1.0-or-later OR Artistic-1.0-Perl
URL:             https://metacpan.org/release/%{base_name}
Source0:         https://cpan.metacpan.org/authors/id/D/DR/DROLSKY/%{base_name}-%{base_version}.tar.gz

BuildArch:       noarch
BuildRequires:   make perl-generators perl-interpreter perl(ExtUtils::MakeMaker) >= 6.76
BuildRequires:   perl(strict) perl(warnings) perl(Carp) perl(constant) perl(Exporter)
BuildRequires:   perl(parent) perl(File::Spec) perl(Test::More) >= 0.96

%description
This module provides functions that are the inverse of built-in perl functions localtime()
and gmtime(). They accept a date as a six-element array, and return the corresponding time(2)
value in seconds since the system epoch (Midnight, January 1, 1970 GMT on Unix, for example).

%package_help

%prep
%autosetup -n %{base_name}-%{base_version}

%build
perl Makefile.PL INSTALLDIRS=vendor NO_PERLLOCAL=1 NO_PACKLIST=1
%make_build

%install
%make_install
%{_fixperms} %{buildroot}/*

%check
make test

%files
%doc README.md
%license LICENSE
%{perl_vendorlib}/*

%files help
%doc Changes CONTRIBUTING.md
%{_mandir}/man3/*

%changelog
* Sun Jan 19 2025 Funda Wang <fundawang@yeah.net> - 2:1.350-2
- drop useless perl(:MODULE_COMPAT) requirement

* Wed Oct 25 2023 wangyuhang <wangyuhang27@huawei.com> - 2:1.350-1
- upgrade to version 1.35

* Wed Oct 26 2022 wangyuhang <wangyuhang27@huawei.com> - 2:1.300-2
- use base_name to opitomize the specfile

* Wed Jul 22 2020 dingyue<dingyue5@huawei.com> - 2:1.300-1
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Sat Dec 21 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:1.280-6
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:change mod of files

* Fri Sep 27 2019 chengquan<chengquan3@huawei.com> - 2:1.280-5
- Type:bugfix
- ID:NA
- SUG:NA
- DESC:merge package

* Mon Sep 16 2019 openEuler Buildteam <buildteam@openeuler.org> - 2:1.280-4
- Package init
